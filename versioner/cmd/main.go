package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"
	"regexp"
	"strings"
	libversion "versioner/pkg/libversion"
)

func main() {
	// Read environment variables
	projectID := os.Getenv("GITLAB_PROJECT_ID")
	gitlabToken := os.Getenv("GITLAB_TOKEN")
	apiName := os.Getenv("API_NAME")
	pipelineId := os.Getenv("PIPELINE_ID")

	// Check if required environment variables are set
	if projectID == "" || gitlabToken == "" {
		log.Fatal("GITLAB_PROJECT_ID and GITLAB_TOKEN must be set in the environment")
	}

	// Get the latest tag version for the API
	latestTag, err := getLatestTag(projectID, gitlabToken, apiName)
	if err != nil {
		log.Fatalf("Error getting latest tag for %s: %v", apiName, err)
	}

	// Determine the version to use based on whether an initial tag exists
	var version string
	if latestTag == "" {
		// Create the initial tag version
		version, err = createInitialVersion(apiName, pipelineId)
		if err != nil {
			log.Fatalf("Error creating initial version for %s: %v", apiName, err)
		}
	} else {
		// Increment the patch version and update commit number
		version, err = libversion.NextReleaseVersion(latestTag, apiName, pipelineId)
		if err != nil {
			log.Fatalf("Error incrementing patch version: %v", err)
		}
	}

	// Tag the commit with the determined version
	if err := createTag(projectID, gitlabToken, version); err != nil {
		log.Printf("Error creating tag for %s: %v", apiName, err)
	}

	fmt.Printf("Tag created for %s: %s\n", apiName, version)

	// Push the tag to the repository
	if err := pushTag(version); err != nil {
		log.Printf("Error pushing tag to repository: %v", err)
	}

	fmt.Printf("Tag %s pushed to the repository\n", version)
}

func createInitialVersion(apiName, pipelineId string) (string, error) {
	// Initially, use version 1.0.0
	version := "1.0.0"

	// If there is a commit number, append it to the version
	if pipelineId != "" {
		version += "-" + apiName + "-" + pipelineId
	}

	return version, nil
}

func getLatestTag(projectID, gitlabToken, apiName string) (string, error) {
	apiURL := fmt.Sprintf("https://gitlab.com/api/v4/projects/%s/repository/tags?private_token=%s", projectID, gitlabToken)
	resp, err := http.Get(apiURL)
	if err != nil {
		return "", fmt.Errorf("Error making HTTP request: %v", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return "", fmt.Errorf("HTTP request failed with status code: %d", resp.StatusCode)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", fmt.Errorf("Error reading response body: %v", err)
	}

	var tags []struct {
		Name string `json:"name"`
	}

	if err := json.Unmarshal(body, &tags); err != nil {
		return "", fmt.Errorf("Error unmarshalling JSON: %v", err)
	}

	for _, tag := range tags {
		if strings.Contains(tag.Name, apiName) {
			// Define a regular expression to match the version pattern
			versionRegex := regexp.MustCompile(`^(\d+\.\d+\.\d+)`)

			// Find the version match in the input string
			matches := versionRegex.FindStringSubmatch(tag.Name)

			// Check if a match is found
			if len(matches) > 1 {
				version := matches[1]
				return version, nil
			}

		}
	}

	return "", nil
}

func createTag(projectID, gitlabToken, tagName string) error {
	apiURL := fmt.Sprintf("https://gitlab.com/api/v4/projects/%s/repository/tags", projectID)

	payload := strings.NewReader(fmt.Sprintf(`{"tag_name": "%s", "ref": "HEAD"}`, tagName))

	req, err := http.NewRequest("POST", apiURL, payload)
	if err != nil {
		return fmt.Errorf("Error creating HTTP request: %v", err)
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Private-Token", gitlabToken)

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return fmt.Errorf("Error making HTTP request: %v", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusCreated {
		responseBody, _ := ioutil.ReadAll(resp.Body)
		return fmt.Errorf("Error creating tag. Status code: %d. Response body: %s", resp.StatusCode, responseBody)
	}

	return nil
}

func pushTag(tagName string) error {
	cmd := exec.Command("git", "push", "origin", tagName)
	_, err := cmd.CombinedOutput()
	if err != nil {
		return nil
	}
	return nil
}
