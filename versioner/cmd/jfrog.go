package main

import (
	"fmt"
	"os"

	"github.com/jfrog/jfrog-client-go/artifactory"
	"github.com/jfrog/jfrog-client-go/artifactory/auth"
	"github.com/jfrog/jfrog-client-go/artifactory/services"
	"github.com/jfrog/jfrog-client-go/config"
	"github.com/jfrog/jfrog-client-go/utils/log"
)

func main() {
	log.SetLogger(log.NewLogger(log.INFO, os.Stdout))
	rtDetails := auth.NewArtifactoryDetails()
	rtDetails.SetUrl("http://192.168.0.4:8081/artifactory")
	rtDetails.SetUser("admin")
	rtDetails.SetPassword("")
	serviceConfig, err := config.NewConfigBuilder().
		SetServiceDetails(rtDetails).
		SetDryRun(false).
		Build()
	if err != nil {
		fmt.Printf("%v", err)
		return
	}

	rtManager, err := artifactory.New(serviceConfig)
	params := services.NewUploadParams()
	params.Pattern = "/Users/sooraj/projects/go/sky-meter/README.md"
	params.Target = "/generic-local/"
	params.Recursive = true
	params.IncludeDirs = false
	params.Regexp = false
	params.Flat = true
	info, _, err := rtManager.UploadFiles(params)
	if err != nil {
		fmt.Printf("Should print this error %v", err)
		return
	}
	fmt.Printf("%v", info)
}
