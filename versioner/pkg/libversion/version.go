package libversion

import (
	"fmt"
	"log"
	"os/exec"
	"strings"
)

func NextReleaseVersion(currentTag, apiName, pipelineId string) (string, error) {
	// Execute git command to get the latest commit message
	cmd := exec.Command("git", "log", "--pretty=format:%s", "-1")
	output, err := cmd.Output()
	if err != nil {
		return "", fmt.Errorf("Error executing git command:", err)
	}

	// Get the latest commit message
	latestCommitMessage := strings.TrimSpace(string(output))

	// Determine the version bump based on the latest commit message
	versionBump := getVersionBump(latestCommitMessage)

	// Increment the version number based on the bump
	newVersion, err := incrementVersion(currentTag, versionBump)
	if err != nil {
		return "", fmt.Errorf("Error executing git command:", err)
	}
	newVersion = newVersion + "-" + apiName + "-" + pipelineId

	log.Println("Latest commit message:", latestCommitMessage)
	log.Println("New version:", newVersion)
	return newVersion, err
}

func getVersionBump(commitMessage string) string {
	switch {
	case strings.HasPrefix(commitMessage, "major:"):
		return "major"
	case strings.HasPrefix(commitMessage, "feat:"):
		return "minor"
	case strings.HasPrefix(commitMessage, "fix:"):
		return "patch"
		// Add more cases as needed for other types of commits
	}

	return "patch" // Default to patch if no specific type is found
}

func incrementVersion(version, bumpType string) (string, error) {
	parts := strings.Split(version, ".")
	if len(parts) != 3 {
		return "", fmt.Errorf("Invalid version format: %s", version)
	}

	switch bumpType {
	case "major":
		parts[0] = incrementNumber(parts[0])
		parts[1] = "0"
		parts[2] = "0"
	case "minor":
		parts[1] = incrementNumber(parts[1])
		parts[2] = "0"
	case "patch":
		parts[2] = incrementNumber(parts[2])
	default:
		return "", fmt.Errorf("Invalid bump type: %s", bumpType)
	}

	return strings.Join(parts, "."), nil
}

func incrementNumber(numStr string) string {
	var num int
	_, err := fmt.Sscanf(numStr, "%d", &num)
	if err != nil {
		return numStr
	}

	num++
	return fmt.Sprintf("%d", num)
}
